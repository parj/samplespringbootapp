[![Build Status](https://travis-ci.com/parj/SampleSpringBootApp.svg?branch=master)](https://travis-ci.com/parj/SampleSpringBootApp)

[![DepShield Badge](https://depshield.sonatype.org/badges/parj/SampleSpringBootApp/depshield.svg)](https://depshield.github.io)

[![FOSSA Status](https://app.fossa.io/api/projects/git%2Bgithub.com%2Fparj%2FSampleSpringBootApp.svg?type=large)](https://app.fossa.io/projects/git%2Bgithub.com%2Fparj%2FSampleSpringBootApp?ref=badge_large)

# What is this

A sample spring boot application. Whilst building the application using `maven`, the project checks the CVE database for vulnerabilities (OWASP is used). In addition - travisci is used for automated builds, plus there is dependency scan is done via DepShield and FOSS status is checked by fossa.io.